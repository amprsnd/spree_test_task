module Spree
  module Admin
    class ImportsController < Spree::Admin::BaseController

      def index

      end

      def create

        u = params[:file]

        # validate filetype
        if u.content_type == 'text/csv' ||
           u.content_type == 'application/vnd.ms-excel'

          # imported file path
          file = Rails.root.join('tmp', u.original_filename)

          # write file
          File.open(file, 'wb') do |f|
            f.write(u.read)
          end

          # start background task
          ImportProductsJob.perform_later file.to_s

          # notice
          flash[:success] = Spree.t(:file_uploaded)
          redirect_to admin_products_url

        else

          # wrong format, redirect to uploading

          flash[:error] = Spree.t(:file_wrong)
          redirect_to admin_imports_url

        end


      end

    end
  end
end

module Spree
  class ImportsController
  end
end