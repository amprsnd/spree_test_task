Spree::Product.class_eval do

  require 'csv'

  # import products Model method for task
  def self.import_products_from_csv filepath

    #time stamp for logs
    time_start = DateTime.now

    # Each file by rows
    CSV.foreach(filepath,
                quote_char: '"',
                col_sep: ';',
                row_sep: :auto,
                headers: true,
                skip_blanks: true) do |row|

      # Each row by cells
      # find empty rows
      row.each do |key, value|

        # start import product to DB if col is not empty
        unless value.nil?

          # add product to DB

          available_on = Date.parse(row[4])
          product = Spree::Product.new(
              name: row[1],
              description: row[2],
              price: (row[3]).gsub(',', '.').to_f,
              available_on: available_on.strftime('%Y-%m-%d %H:%M:%S'),
              slug: row[5],
              shipping_category_id: 1, # default value
          )

          # add category
          category = Spree::Taxon.find_or_create_by(name: row[7])
          product.taxons << category

          product.save

          break
        end

      end

    end

    # delete file
    File.delete(filepath)

    #time stamp for logs
    time_end = DateTime.now

    # log report
    puts '==================='
    puts "CSV import complete. Start time: #{time_start}, end time: #{time_end}"
    puts '==================='

    end

end