class ImportProductsJob < ApplicationJob
  queue_as :default

  def perform(filepath)

    Spree::Product.import_products_from_csv filepath

  end

end
